#ifndef WET_2_BANK_H
#define WET_2_BANK_H

#include <list>
#include <string>
#include <pthread.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <cmath>

#define BANK_ID -1
#define BANK_PASSWORD 0
#define BANK_INITIAL_BALANCE 0
#define UPPER_COMMISSION 4
#define LOWER_COMMISSION 2

using std::list;
using std::string;
using std::ofstream;
using std::to_string;
using std::min;

class ATM;
class Bank;

class Account {
    public:

    // account data
    // data that is const is unwritable - so there is no need to use the locks when reading it (can be read by any thread anytime)
    const int id;
    const int password;
    int balance;

    // locks data
    pthread_mutex_t rd_lock;
    pthread_mutex_t wr_lock;
    int num_of_readers;

      Account(int id, int password, int balance): id(id), password(password), balance(balance), num_of_readers(0) {

        pthread_mutex_init(&rd_lock, nullptr); // init to unlocked
        pthread_mutex_init(&wr_lock, nullptr); // init to unlocked

    }
    bool  operator< (Account a){ //sorting operator for list.sort. return true if id of the first account is smaller then the second one
        return this->id <a.id;
    }
};

class Bank {
public:

    list<Account> account_list;

    string path_to_log;

    pthread_mutex_t account_list_rd_lock; // rd\wr locks for the bank's accounts data structures
    pthread_mutex_t account_list_wr_lock;
    int account_list_num_of_readers;

    pthread_mutex_t log_wr_lock; // wr lock for the log

    Account banks_account;

    bool isFinish;// indicate if all the atm finish to run

    pthread_mutex_t isFinish_rd_lock;
    pthread_mutex_t isFinish_wr_lock;
    int isFinish_num_of_readers;



    Bank(): account_list_num_of_readers(0),banks_account(BANK_ID, BANK_PASSWORD, BANK_INITIAL_BALANCE) ,isFinish(false){

        path_to_log = "./log.txt";

        // init bank DS mutexs to unlocked
        pthread_mutex_init(&account_list_rd_lock, nullptr);
        pthread_mutex_init(&account_list_wr_lock, nullptr);
        pthread_mutex_init(&log_wr_lock, nullptr);

    }

// bank internal functions

    // this function reads from the list, so it must be called when the list's rd_lock\wr_lock is locked.
    // we don't need to use the account's locks because we only read the id member, which is protected (non-writable).
    Account* get_account(int id) {

        for (std::list<Account>::iterator it=account_list.begin(); it != account_list.end(); ++it){
            if(it->id==id){
                return &(*it);
            }
        }
        return nullptr;
    }

    // FIXME concurrency test this function
    // locks the given account for a read operation, according to readers-writers algorithm
    void lock_account_for_reading(Account* account) {

        pthread_mutex_lock(&(account->rd_lock));
        account->num_of_readers++;
        if (account->num_of_readers == 1) {
            pthread_mutex_lock(&(account->wr_lock));
        }
        pthread_mutex_unlock(&(account->rd_lock));

    }

    // FIXME concurrency test this function
    // unlocks the given account after a read operation, according to readers-writers algorithm
    void unlock_account_after_reading(Account* account) {

        pthread_mutex_lock(&(account->rd_lock));
        account->num_of_readers--;
        if (account->num_of_readers == 0) {
            pthread_mutex_unlock(&(account->wr_lock));
        }
        pthread_mutex_unlock(&(account->rd_lock));

    }

    // FIXME concurrency test this function
    // locks the account list for a read operation, according to readers-writers algorithm
    void lock_list_for_reading() {

        pthread_mutex_lock(&account_list_rd_lock);
        account_list_num_of_readers++;
        if (account_list_num_of_readers == 1) {
            pthread_mutex_lock(&account_list_wr_lock);
        }
        pthread_mutex_unlock(&account_list_rd_lock);

    }

    // FIXME concurrency test this function
    // unlocks the account list after a read operation, according to readers-writers algorithm
    void unlock_list_after_reading() {

        pthread_mutex_lock(&account_list_rd_lock);
        account_list_num_of_readers--;
        if (account_list_num_of_readers == 0) {
            pthread_mutex_unlock(&account_list_wr_lock);
        }
        pthread_mutex_unlock(&account_list_rd_lock);

    }
    // locks the isFinish flag for a read operation, according to readers-writers algorithm
    void lock_isFinish_for_reading() {

        pthread_mutex_lock(&isFinish_rd_lock);
        isFinish_num_of_readers++;
        if (isFinish_num_of_readers == 1) {
            pthread_mutex_lock(&isFinish_wr_lock);
        }
        pthread_mutex_unlock(&isFinish_rd_lock);

    }
    // unlocks the isFinish flag after a read operation, according to readers-writers algorithm
    void unlock_isFinish_after_reading() {

        pthread_mutex_lock(&isFinish_rd_lock);
        isFinish_num_of_readers--;
        if (isFinish_num_of_readers == 0) {
            pthread_mutex_unlock(&isFinish_wr_lock);
        }
        pthread_mutex_unlock(&isFinish_rd_lock);

    }
    //set isFinish flag to true using mutex.
    void set_isFinish_to_true(){
        pthread_mutex_lock(&isFinish_wr_lock);
        isFinish=true;
        pthread_mutex_unlock(&isFinish_wr_lock);
    }
    //return isFinish flag
    bool get_isFinish(){
        this->lock_isFinish_for_reading();
        bool is_finish=isFinish;
        this->unlock_isFinish_after_reading();
        return is_finish;
    }

    // the function prints the given line to the log
    void print_to_log(string msg) {

        pthread_mutex_lock(&log_wr_lock);

        ofstream log;
        log.open(path_to_log, std::ofstream::out | std::ofstream::app);
        log << msg << "\n";
        log.close();

        pthread_mutex_unlock(&log_wr_lock);

    }



// bank API functions (for use by ATMs)
    void create_account(int account_id, int password, int initial_balance, int atm_id) {

        pthread_mutex_lock(&account_list_wr_lock);

        if (get_account(account_id) != nullptr) {
            print_to_log("Error " + to_string(atm_id) + ": Your transaction failed – account with the same id exists");
        } else {
            Account new_account(account_id, password, initial_balance);
            account_list.push_back(new_account);
            print_to_log(to_string(atm_id) + ": New account id is " + to_string(account_id) + " with password " + to_string(password) + " and initial balance " + to_string(initial_balance));
        }

        sleep(1);
        pthread_mutex_unlock(&account_list_wr_lock);

    }

    void deposit(int account_id, int password, int amount, int atm_id) {

        lock_list_for_reading(); // add reader

        Account* account = get_account(account_id);  // get_account does not access vulnerable variables, no locks needed
        if (account == nullptr) {

            print_to_log("Error "+to_string(atm_id)+": Your transaction failed – account id "+to_string(account_id)+" does not exist");
            sleep(1);

        } else {

            // lock account for writing
            pthread_mutex_lock(&(account->wr_lock));

            if (account->password != password) {
                print_to_log("Error "+to_string(atm_id)+": Your transaction failed – password for account id "+to_string(account_id)+" is incorrect");
            } else {
                account->balance+=amount;
                print_to_log(to_string(atm_id)+": Account "+to_string(account_id)+" new balance is "+to_string(account->balance)+" after "+to_string(amount)+" $ was deposited");
            }

            sleep(1);
            pthread_mutex_unlock(&(account->wr_lock));;
        }

        unlock_list_after_reading(); // remove reader
    }

    void withdrawal(int account_id, int password, int amount, int atm_id) {

        lock_list_for_reading(); // add reader

        Account* account = get_account(account_id);  // get_account does not access vulnerable variables, no locks needed
        if (account == nullptr) {

            print_to_log("Error "+to_string(atm_id)+": Your transaction failed – account id "+to_string(account_id)+" does not exist");
            sleep(1);

        } else {

            // lock account for writing
            pthread_mutex_lock(&(account->wr_lock));

            if (account->password != password) {
                print_to_log("Error "+to_string(atm_id)+": Your transaction failed – password for account id "+to_string(account_id)+" is incorrect");
            } else {
                if (account->balance < amount) {
                    print_to_log("Error "+to_string(atm_id)+": Your transaction failed – account id "+to_string(account_id)+" balance is lower than "+to_string(amount));
                } else {
                    account->balance-=amount;
                    print_to_log(to_string(atm_id)+": Account "+to_string(account_id)+" new balance is "+to_string(account->balance)+" after "+to_string(amount)+" $ was withdrew");
                }
            }

            sleep(1);
            pthread_mutex_unlock(&(account->wr_lock));
        }

        unlock_list_after_reading(); // remove reader
    }

    void get_balance(int account_id, int password, int atm_id) {

        lock_list_for_reading(); // add reader

        Account* account = get_account(account_id);  // get_account does not access vulnerable variables, no locks needed
        if (account == nullptr) {

            print_to_log("Error "+to_string(atm_id)+": Your transaction failed – account id "+to_string(account_id)+" does not exist");
            sleep(1);

        } else {

            // lock account for reading
            lock_account_for_reading(account);

            if (account->password != password) {
                print_to_log("Error "+to_string(atm_id)+": Your transaction failed – password for account id "+to_string(account_id)+" is incorrect");
            } else {
                print_to_log(to_string(atm_id)+": Account "+to_string(account_id)+" balance is "+to_string(account->balance));
            }

            sleep(1);
            unlock_account_after_reading(account);
        }

        unlock_list_after_reading(); // remove reader
    }

    void transaction(int src_account_id, int password , int dst_account_id, int amount, int atm_id) {

        lock_list_for_reading(); // add reader

        Account* src_account = get_account(src_account_id);  // get_account does not access vulnerable variables, no locks needed
        Account* dst_account = get_account(dst_account_id);  // get_account does not access vulnerable variables, no locks needed

        if (src_account == nullptr) {
            print_to_log("Error "+to_string(atm_id)+": Your transaction failed – account id "+to_string(src_account_id)+" does not exist");
            sleep(1);
        } else {

            if (dst_account == nullptr) {
                print_to_log("Error " + to_string(atm_id) + ": Your transaction failed – account id " +
                             to_string(dst_account_id) + " does not exist");
                sleep(1);
            } else {
                if (src_account_id != dst_account_id) {

                    // lock both accounts for writing, smaller id first
                    if (min(src_account_id, dst_account_id) == src_account_id) {
                        pthread_mutex_lock(&(src_account->wr_lock));
                        pthread_mutex_lock(&(dst_account->wr_lock));
                    } else {
                        pthread_mutex_lock(&(dst_account->wr_lock));
                        pthread_mutex_lock(&(src_account->wr_lock));
                    }

                    if (src_account->password != password) {
                        print_to_log(
                                "Error " + to_string(atm_id) + ": Your transaction failed – password for account id " +
                                to_string(src_account_id) + " is incorrect");
                    } else {
                        if (src_account->balance < amount) {
                            print_to_log("Error " + to_string(atm_id) + ": Your transaction failed – account id " +
                                         to_string(src_account_id) + " balance is lower than " + to_string(amount));
                        } else {
                            src_account->balance -= amount;
                            dst_account->balance += amount;
                            print_to_log(to_string(atm_id) + ": Transfer " + to_string(amount) + " from account " +
                                         to_string(src_account_id) + " to account " + to_string(dst_account_id) +
                                         " new account balance is " + to_string(src_account->balance) +
                                         " new target account balance is " + to_string(dst_account->balance));
                        }
                    }

                    sleep(1);
                    pthread_mutex_unlock(&(src_account->wr_lock));  // when unlocking order does not matter
                    pthread_mutex_unlock(&(dst_account->wr_lock));
                }
            }
        }

        unlock_list_after_reading(); // remove reader
    }

    void takeCommission(int commission_rate){
        lock_list_for_reading(); // lock list for reading
        for (std::list<Account>::iterator it=account_list.begin(); it != account_list.end(); ++it) {
            pthread_mutex_lock(&(it->wr_lock)); //lock account for writing
            int commission=(int)round(it->balance*(commission_rate/100.0));
            it->balance-=commission;
            banks_account.balance+=commission;
            print_to_log("Bank: commissions of "+ to_string(commission_rate) +" % were charged, the bank gained "+to_string(commission)+" $ from account "+to_string(it->id));
            pthread_mutex_unlock(&(it->wr_lock)); //unlock account
        }
        unlock_list_after_reading();
    }

    void printStatus(){
        //FIXME : unlock account_list_wr_lock only at the end of function
        pthread_mutex_lock(&account_list_wr_lock); // lock account for writing: sorting the list, and keep locking so no one will add an account after sorting
        account_list.sort(); //sorting list
        std::cout << "\033[2J\033[1;1H";//cleaning the screen and print from top left corner
        std::cout<<"Current Bank Status"<<std::endl;
        for (std::list<Account>::iterator it=account_list.begin(); it != account_list.end(); ++it) {
            std::cout<<"Account "<<it->id<<": Balance – "<<it->balance<<" $ , Account Password – "<<it->password<<" "<<std::endl;
        }
        pthread_mutex_unlock(&account_list_wr_lock);
    }

};

void* takeCommissionThread (void* thread_bank){
    Bank* bank=(Bank*)thread_bank;
    while(!bank->get_isFinish()){
        sleep(3);
        int commission_rate=(rand()%(UPPER_COMMISSION-LOWER_COMMISSION+1))+LOWER_COMMISSION; //commission_rate will be a random number between LOWER_COMMISSION to UPPER_COMMISSION
        bank->takeCommission(commission_rate);
    }
    pthread_exit(NULL);
}

void* printStatusThread (void* thread_bank){
    Bank* bank=(Bank*)thread_bank;
    while(!bank->get_isFinish()){
        usleep(500000);
        bank->printStatus();
    }
    pthread_exit(NULL);
}


#endif //WET_2_BANK_H
