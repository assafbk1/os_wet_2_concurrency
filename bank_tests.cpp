
#include "bank_tests.h"
#include "bank.h"
#include <iostream>
#include <cassert>


using std::cout;
using std::endl;


void test_is_account_exists() {

    Bank bank = Bank();
    int password = 1234;
    int balance = 0;

    assert(bank.get_account(10) == nullptr); // sanity test

    // add accounts manually
    Account new_account_1 = Account(10, password, balance);
    Account new_account_2 = Account(11, password, balance);
    Account new_account_3 = Account(12, password, balance);
    bank.account_list.push_back(new_account_1);
    bank.account_list.push_back(new_account_2);
    bank.account_list.push_back(new_account_3);

    assert(bank.get_account(10)->id == 10);
    assert(bank.get_account(12)->id == 12);
    assert(bank.get_account(99) == nullptr);
}

void test_create_account() {

    Bank bank = Bank();
    int password = 1234;
    int balance = 0;
    int atm_id = 50;

    assert(bank.get_account(10) == nullptr); // sanity test

    // create new accounts
    bank.create_account(10, password, balance, atm_id);
    bank.create_account(11, password, balance, atm_id);
    bank.create_account(12, password, balance, atm_id);

    // create an account that already exists [we will see this in the log]
    bank.create_account(10, password, balance, atm_id);

    assert(bank.get_account(10)->id == 10);
    assert(bank.get_account(12)->id == 12);
    assert(bank.get_account(99) == nullptr);

}

void test_print_to_log() {

    Bank bank = Bank();

    bank.print_to_log("blablabla");
    bank.print_to_log("blablabla");

}

void test_deposit() {

    Bank bank = Bank();
    int id = 10;
    int password = 1234;
    int balance = 0;
    int atm_id = 50;

    bank.create_account(id-1, password, balance, atm_id);
    bank.create_account(id, password, balance, atm_id);
    bank.create_account(id+1, password, balance, atm_id);


    // non-existing account
    bank.deposit(999, password, 100, atm_id);
    assert(bank.get_account(999) == nullptr);

    // bad password
    bank.deposit(id, 9999, 100, atm_id);
    assert(bank.get_account(id)->balance == 0);

    // successful deposit
    bank.deposit(id, password, 100, atm_id);
    assert(bank.get_account(id)->balance == 100);


}

void test_withdrawal() {

    Bank bank = Bank();
    int id = 10;
    int password = 1234;
    int balance = 100;
    int atm_id = 50;

    bank.create_account(id-1, password, balance, atm_id);
    bank.create_account(id, password, balance, atm_id);
    bank.create_account(id+1, password, balance, atm_id);


    // non-existing account
    bank.withdrawal(999, password, 80, atm_id);
    assert(bank.get_account(999) == nullptr);

    // bad password
    bank.withdrawal(id, 9999, 80, atm_id);
    assert(bank.get_account(id)->balance == balance);

    // amount too high
    bank.withdrawal(id, password, 300, atm_id);
    assert(bank.get_account(id)->balance == balance);

    // successful deposit
    bank.withdrawal(id, password, 80, atm_id);
    assert(bank.get_account(id)->balance == 20);

}

void test_get_balance() {

    Bank bank = Bank();
    int id = 10;
    int password = 1234;
    int balance = 100;
    int atm_id = 50;

    bank.create_account(id-1, password, balance, atm_id);
    bank.create_account(id, password, balance, atm_id);
    bank.create_account(id+1, password, balance, atm_id);


    // non-existing account
    bank.get_balance(999, password, atm_id);
    assert(bank.get_account(999) == nullptr);

    // bad password
    bank.get_balance(id, 9999, atm_id);

    // successful get balance
    bank.get_balance(id, password, atm_id);

    // test results for this are only in the log
}

void test_transaction() {

    Bank bank = Bank();
    int src_id = 10;
    int dst_id = 12;
    int password = 1234;
    int balance = 100;
    int amount = 30;
    int atm_id = 50;

    bank.create_account(src_id, password, balance, atm_id);
    bank.create_account(74, password, balance, atm_id);
    bank.create_account(dst_id, password, balance, atm_id);

    // non-existing src account
    bank.transaction(998, dst_id, password, amount, atm_id);

    // non-existing dst account
    bank.transaction(src_id, 999, password, amount, atm_id);

    // bad password
    bank.transaction(src_id, dst_id, 9999, amount, atm_id);
    assert(bank.get_account(src_id)->balance == balance);
    assert(bank.get_account(dst_id)->balance == balance);

    // amount too high
    bank.transaction(src_id, dst_id, password, 300, atm_id);
    assert(bank.get_account(src_id)->balance == balance);
    assert(bank.get_account(dst_id)->balance == balance);

    // successful deposit
    bank.transaction(src_id, dst_id, password, amount, atm_id);
    assert(bank.get_account(src_id)->balance == balance-amount);
    assert(bank.get_account(dst_id)->balance == balance+amount);

}

void test_mutex() {

//    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t mutex;
    int res = pthread_mutex_init(&mutex, nullptr);
    pthread_mutex_lock(&mutex);
    cout << "bla\n";
    pthread_mutex_unlock(&mutex);
    pthread_mutex_lock(&mutex);
    pthread_mutex_lock(&mutex);
    cout << "bla\n";
}

void bank_create(Bank* bank){

    int password = 1234;
    int balance = 100;
    int atm_id = 50;

    assert(bank->get_account(10) == nullptr); // sanity test

    // create new accounts
    bank->create_account(11, password, balance++, atm_id);
    bank->create_account(12, password, balance++, atm_id);
    bank->create_account(10, password, balance++, atm_id);

    // create an account that already exists [we will see this in the log]
    bank->create_account(10, password, balance, atm_id);

    assert(bank->get_account(10)->id == 10);
    assert(bank->get_account(12)->id == 12);
    assert(bank->get_account(99) == nullptr);
};

void test_commision(){
    Bank bank = Bank();
    bank_create(&bank);
    bank.takeCommission(3);
    sleep(3);
    bank.create_account(5, 1111, 100, 2);
    bank.takeCommission(2);

}

void test_getStatus(){
    Bank bank = Bank();
    bank_create(&bank);
    bank.printStatus();
    sleep(3);
    bank.create_account(5, 1111, 100, 2);
    bank.printStatus();

}




void run_bank_tests() {

//    test_mutex();
//    test_print_to_log();

    test_is_account_exists();
    test_create_account();
    test_deposit();
    test_withdrawal();
    test_get_balance();
    test_transaction();
    test_commision();
    test_getStatus();


    cout << "passed all tests" << endl;

}