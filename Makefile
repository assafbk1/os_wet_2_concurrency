# Makefile for the Bank program
CXX = g++
CXXFLAGS = -std=c++11 -Wall
CCLINK = $(CXX)
LINKS = -lpthread
OBJS = main.o
RM = rm -f
# Creating the  executable
Bank: $(OBJS)
	$(CCLINK) -o Bank $(OBJS) $(LINKS)
# Creating the object files
main.o: main.cpp bank.h  atm.h
# Cleaning old files before new make
clean:
	$(RM) $(TARGET) *.o *~ "#"* core.*
