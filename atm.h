//
// Created by os on 22.12.2019.
//

#ifndef OS_WET_2_CONCURRENCY_ATM_H
#define OS_WET_2_CONCURRENCY_ATM_H

#include <vector>
using std::ifstream;

typedef struct thread_atm_data {
    Bank* bank;
    char* file;
    int atm_id;

    thread_atm_data(): bank(NULL),file(NULL),atm_id(0){}
} thread_atm_data;


void* atmFunction(void* data){
    thread_atm_data* atm_data= (thread_atm_data*) data;
    ifstream file(atm_data->file);
    if(!file||!file.good()){//file does not exist or some other error
      perror("Error: ") ;
      exit(-1);
    }
    string cmd;

    while (getline(file, cmd)) {

        std::vector<string> command_arg; // will contain the arguments of the cmd
        string word;
        for(unsigned i=0;i<cmd.size();i++){
            if(cmd[i]!=' ')
                word+=cmd[i];
            else{
                command_arg.push_back(word);
                word="";
            }
        }
        command_arg.push_back(word); //push last argument

        switch(command_arg[0][0]) {
            case 'O':
                atm_data->bank->create_account(stoi(command_arg[1]),stoi(command_arg[2]),stoi(command_arg[3]),atm_data->atm_id);
                break;
            case 'D':
                atm_data->bank->deposit(stoi(command_arg[1]),stoi(command_arg[2]),stoi(command_arg[3]),atm_data->atm_id);
                break;
            case 'W':
                atm_data->bank->withdrawal(stoi(command_arg[1]),stoi(command_arg[2]),stoi(command_arg[3]),atm_data->atm_id);
                break;
            case 'B':
                atm_data->bank->get_balance(stoi(command_arg[1]),stoi(command_arg[2]),atm_data->atm_id);
                break;
            case 'T':
                atm_data->bank->transaction(stoi(command_arg[1]),stoi(command_arg[2]),stoi(command_arg[3]),stoi(command_arg[4]),atm_data->atm_id);
                break;
            default:
                break;
        }
        usleep(100000);
    }

    pthread_exit(NULL);
}

#endif //OS_WET_2_CONCURRENCY_ATM_H
