
#include <iostream>
#include <pthread.h>
#include <fstream>
#include "bank.h"
#include <string.h>
#include "atm.h"

int main(int argc, char **argv) {
    bool valid_input=true; //assuming input is valid
    if(argc<3){//not enough arguments
        valid_input=false;
    }
    int atm_num=  atoi(argv[1]);
    if(atm_num<1){//num of atm is illegal
        valid_input=false;
    }

    if(valid_input) {
        Bank *bank = new Bank();//creating the bank that will be shred by all threads
        pthread_t threads[atm_num + 2]; //atm_num threads for atm and 2 for the bank
        thread_atm_data data[atm_num]; //

        for (int i = 0; i < atm_num; i++) { //creating atm threads
            data[i].bank = bank;
            data[i].file = argv[i + 2];
            data[i].atm_id = i + 1;

            if(pthread_create(&threads[i], NULL, atmFunction, (void *) &data[i])){
                perror("Error:");//pthread_create failed
                exit(0);
            }
        }
        if(pthread_create(&threads[atm_num], NULL, printStatusThread, (void *) bank)){//create print account status thread
            perror("Error:");//pthread_create failed
            exit(0);
        }

        if(pthread_create(&threads[atm_num+1], NULL, takeCommissionThread, (void *) bank)){//create commissions thread
            perror("Error:");//pthread_create failed
            exit(0);
        }

        for (int i = 0; i < atm_num + 2; i++) { //waiting for threads to finish
            if (i == atm_num) {//meaning all atm finish to run
                bank->set_isFinish_to_true();
            }
            pthread_join(threads[i], NULL);
        }

        delete bank;
    }
    else
        std::cout<<"illegal arguments"<<std::endl;
    return 0;
}